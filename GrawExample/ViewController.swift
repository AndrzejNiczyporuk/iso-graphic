//
//  ViewController.swift
//  GrawExample
//
//  Created by programista on 17.02.2016.
//  Copyright © 2016 programista. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
    var grenSq :UIView?
    var redSq :UIView?
    var animator1 :UIDynamicAnimator!
    var animator2 :UIDynamicAnimator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var dimen = CGRectMake(25, 25, 50, 50)
        grenSq = UIView(frame:dimen)
        grenSq?.backgroundColor  = UIColor.greenColor()
        
        
        dimen = CGRectMake(120, 25, 40, 40)
        redSq = UIView(frame:dimen)
        redSq?.backgroundColor  = UIColor.redColor()
        
        
        self.view.addSubview(grenSq!)
        self.view.addSubview(redSq!)
        
        //INITIALIZE animator
        animator1 = UIDynamicAnimator(referenceView: self.view)
        animator2 = UIDynamicAnimator(referenceView: self.view)
        
        //add gravity 
        let gravity1 = UIGravityBehavior(items: [grenSq!])
        let gravity2 = UIGravityBehavior(items: [redSq!])
        
        let direct1 = CGVectorMake(0.0, 0.1)
        let direct2 = CGVectorMake(0.1, 0.5)
        
        
        gravity1.gravityDirection = direct1
        gravity2.gravityDirection = direct2
       // gravity.increaseSize(redSq)
    
        // collision
        let boundries2 = UICollisionBehavior(items: [redSq!])
        boundries2.translatesReferenceBoundsIntoBoundary = true
        
        let boundries1 = UICollisionBehavior(items: [grenSq!])
        boundries1.translatesReferenceBoundsIntoBoundary = true
        
        //elastic 
        let bounce1 = UIDynamicItemBehavior(items: [grenSq!])
        bounce1.elasticity = 0.5
        
        let bounce2 = UIDynamicItemBehavior(items: [redSq!])
        bounce2.elasticity = 0.5
        
        
        animator1?.addBehavior(bounce1)
        animator1?.addBehavior(boundries1)
        animator1?.addBehavior(gravity1)
        
        
        animator2?.addBehavior(bounce2)
        animator2?.addBehavior(boundries2)
        animator2?.addBehavior(gravity2)
        
    }




}

